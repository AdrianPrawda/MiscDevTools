:: Runs CSVBT in the HTools virtual environment
@ECHO OFF

:: Config beginn
SETLOCAL
SET VENV_FPATH=D:\Virtualenv\htools
:: Config end

ECHO Switching to virtual env
CALL %VENV_FPATH%\Scripts\activate.bat
IF %ERRORLEVEL% NEQ 0 GOTO err_activate_venv
ECHO OK
ECHO.

ECHO Installing dependencies
ECHO.
Python setup.py develop
IF %ERRORLEVEL% NEQ 0 GOTO err_develop
ECHO OK
ECHO.

ECHO Starting CSVBT
ECHO.
Python csvbt.py
IF %ERRORLEVEL% NEQ 0 GOTO err_exec
ECHO.
ECHO OK
GOTO exit

:err_activate_venv
ECHO Could not activate python virtual environment
GOTO err_exit

:err_develop
ECHO Could not develop dependencies
GOTO err_exit

:err_exec
ECHO Could not run CSVBT

:err_exit
PAUSE
EXIT /B 1

:exit
PAUSE
EXIT /B 0
