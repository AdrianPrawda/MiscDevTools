# CSV Browser Tool (or CSVBT for short)


Loads a CSV in the format "Title";"URL" and allows the user to step through it and open links with a click of a button in a browser of their choice.
Made for a very specific use case (and user)


**Version 0.1**


## Known Bugs

 * There is a random, unused input box under the status bar if you resize the window
 * Chrome opens every url twice

 
## Planed features

 * Implement IE / Edge / Stanard Browser support and test Opera support
 * Implement Linux / MAC support
 * Build a standalone exe and embedd graphics