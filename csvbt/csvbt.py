#!/usr/bin/python

import os
import pickle
import webbrowser
import winreg
import subprocess

from tkinter import *
from tkinter import filedialog
from tkinter import ttk


class CSVProxy:

    def __init__(self, f, delim):
        self.fh = f
        self.delim = delim

    def __iter__(self):
        return self

    def __next__(self):
        s = self.fh.readline()
        if s == "":
            raise StopIteration

        so = s.split(self.delim)

        for i in range(len(so)):
            x = so[i]
            if x.startswith('"'):
                x = x[1:]
            if x.endswith('"'):
                x = x[:-1]
            so[i] = x
        return so


class WebbrowserProxy:

    def __init__(self):
        self.registry = {}

    def register(self, name, path):
        print("Registered %s at %s" % (name, path))
        self.registry[name] = path

    def get(self, name):
        if name in self.registry:
            return webbrowser.get(self.registry[name])
        return webbrowser.get(name)

    def get_path(self, name):
        if name in self.registry:
            return self.registry[name]
        return None

    def open_in_new_tab(self, name, url):
        return self.get(name).open_new_tab(url)


class PersistantStorage:

    def __init__(self):
        self.fname = ""
        self.browser = "Firefox"
        self.entry = 0


class MainWindow(ttk.Frame):

    def __init__(self, master=None):
        # Configure persistant storage
        self.ps = self._init_persistant_storage("gui.cfg")

        # CSV list data
        self.csv_index = self.ps.entry
        self.csv_max = 0
        self.csv_data = []
        self.csv_open = False

        # Webbrowser proxy
        self.webbrowser = self._init_webbrowser()

        # Configure and save parent
        self.master = master

        # Init super (main content frame) and configure
        super().__init__(master, padding="3 3 12 12")
        self.grid(column=0, row=0, sticky=(N, W, E, S))
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        # Helper frames
        self.f_ctrl = self._init_frame(self, 0, 4, 4, 1)

        # Images
        self.img_next = PhotoImage(file="next.gif")
        self.img_prev = PhotoImage(file="prev.gif")

        # Text variables
        self.tkv_file = StringVar(self.master, value="Path to CSV")
        self.tkv_prev = StringVar(self.master, value="Previous: -")
        self.tkv_curr = StringVar(self.master, value="Current: -")
        self.tkv_next = StringVar(self.master, value="Next: -")
        self.tkv_stat = StringVar(self.master, value="Status: -")
        self.tkv_page = StringVar(self.master, value="Page: -/-")
        self.tkv_browser = StringVar(self.master, value="Firefox")

        # Buttons
        self.btn_next = self._init_img_btn(self._cb_next, self.img_next, self.f_ctrl, 2, 4)
        self.btn_prev = self._init_img_btn(self._cb_prev, self.img_prev, self.f_ctrl, 0, 4)
        self.btn_open = self._init_btn(self._cb_open, "Open", self.f_ctrl, 1, 4)
        self.btn_load = self._init_btn(self._cb_load, "Load", self, 3, 0)
        self.btn_file = self._init_btn(self._cb_file, "Browse", self, 1, 0)

        # Labels
        self.lbl_prev = self._init_lbl(self.tkv_prev, 0, 1)
        self.lbl_curr = self._init_lbl(self.tkv_curr, 0, 2)
        self.lbl_next = self._init_lbl(self.tkv_next, 0, 3)
        self.lbl_stat = self._init_lbl(self.tkv_stat, 0, 6)
        self.lbl_page = self._init_lbl(self.tkv_page, 0, 5)

        # Inputs
        self.ddl_browsers = self._init_ddl({"Firefox", "Chrome", "Opera"}, "Firefox", 2, 0)
        self.inp_file = self._init_inp(0, 0, self.tkv_file)

        # Finalize
        self._finalize_gui()
        self._finalize_system()

    #
    # GUI and process inits
    #

    def _init_ddl(self, options, default, column=0, row=0):
        self.tkv_browser.set(default)

        ddl = OptionMenu(self, self.tkv_browser, *options)
        ddl.grid(row=row, column=column)
        return ddl

    def _init_inp(self, column=0, row=0, textvar=None):
        inp = ttk.Entry(self)
        if textvar is not None:
            inp = ttk.Entry(self, textvariable=textvar)
        inp.grid(row=row, column=column)
        return inp

    def _init_frame(self, master, column=0, row=0, columnspan=1, rowspan=1):
        frame = ttk.Frame(master)
        frame.grid(row=row, column=column, columnspan=columnspan, rowspan=rowspan)
        return frame

    def _init_btn(self, cmd, text, master, column=0, row=0):
        btn = ttk.Button(master, command=cmd, text=text)
        btn.grid(row=row, column=column)
        return btn

    def _init_img_btn(self, cmd, img, master, column=0, row=0):
        btn = ttk.Button(master, command=cmd)
        btn.config(image=img, width="32")
        btn.grid(row=row, column=column)
        return btn

    def _init_lbl(self, text, column=0, row=0):
        lbl = ttk.Label(self, textvariable=text)
        if type(lbl) is str:
            lbl = ttk.Label(self, text=text)
        lbl.grid(row=row, column=column, columnspan=4)
        return lbl

    def _init_persistant_storage(self, fn):
        fname = os.getcwd() + "\\%s" % fn

        if os.path.isfile(fname):
            with open(fname, 'rb') as f:
                return pickle.load(f)
        else:
            with open(fname, 'w+') as f:
                return PersistantStorage()

    def _init_webbrowser(self):
        wproxy = WebbrowserProxy()

        # Registry paths
        ff_reg_path = "SOFTWARE\\Classes\\Applications\\firefox.exe\\shell\\open\\command"
        chrome_reg_path = "SOFTWARE\\Classes\\ChromeHTML\\Application"

        # Read registry
        ff_res = self._read_reg_key(ff_reg_path)
        chrome_res = self._read_reg_key(chrome_reg_path, "ApplicationIcon")

        # Clean up
        ff_path = ff_res[0].split('"')[1] + " %s".replace("\\", "/")
        chrome_path = chrome_res[0].split(",")[0] + " %s".replace("\\", "/")

        # Register
        wproxy.register('firefox', ff_path)
        wproxy.register('mozilla', ff_path)
        wproxy.register('netscape', ff_path)
        wproxy.register('chrome', chrome_path)
        wproxy.register('google-chrome', chrome_path)

        return wproxy

    #
    # Finalizers
    #

    def _finalize_gui(self):
        # Add padding
        for child in self.winfo_children():
            child.grid_configure(padx=5, pady=5)
        for child in self.f_ctrl.winfo_children():
            child.grid_configure(padx=5, pady=5)

        # Check persistant storage
        if self.ps.fname != "":
            self.tkv_file.set(self.ps.fname)
        if self.ps.browser != "Firefox":
            self.tkv_browser.set(self.ps.browser)

        # Set protocol handler
        self.master.protocol("WM_DELETE_WINDOW", self._cb_on_exit)

        # Set focus
        self.btn_load.focus()

    def _finalize_system(self):
        # check if valid file was stored, if so, auto-load file
        if os.path.isfile(self.ps.fname):
            self._cb_load()

    #
    # Helper methods
    #

    def _load_value(self, key):
        return getattr(self.ps, key, None)


    def _save_value(self, key, value):
        setattr(self.ps, key, value)


    def _save_persistant_storage(self, fn):
        fname = os.getcwd() + "\\%s" % fn
        with open(fname, 'wb') as f:
            pickle.dump(self.ps, f)

    def _set_status(self, text, mode=None):
        if mode == "ERROR":
            self.lbl_stat.config(foreground="red")
        elif mode == "WARN":
            self.lbl_stat.config(foreground="yellow")
        elif mode == "INFO":
            self.lbl_stat.config(foreground="blue")
        elif mode == "SUCCESS":
            self.lbl_stat.config(foreground="green")
        else:
            self.lbl_stat.config(foreground="black")
        self.tkv_stat.set(text)

    def _update_csv_info_labels(self):
        # check if csv is loaded
        if not self.csv_open:
            self._set_status("Oops, something went wrong loading the CSV file", "ERROR")
            return

        # set page and current labels (and fix encodings)
        str_curr = self._fix_encoding(self.csv_data[self.csv_index][0])
        self.tkv_page.set("Page: %d/%d" % (self.csv_index + 1, self.csv_max))
        self.tkv_curr.set("Current: %s" % str_curr)

        # get next and previous
        str_next = "-"
        str_prev = "-"
        if self.csv_index != 0:
            str_prev = self.csv_data[self.csv_index-1][0]
        if self.csv_index != self.csv_max - 1:
            str_next = self.csv_data[self.csv_index+1][0]

        # Fix encodings
        str_next = self._fix_encoding(str_next)
        str_prev = self._fix_encoding(str_prev)

        # set next and previous labels
        self.tkv_prev.set("Previous: %s" % str_prev)
        self.tkv_next.set("Next: %s" % str_next)

    def _read_reg_key(self, path, _filter=None):
        out = []

        try:
            explorer = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path)
            c = 0
            while True:
                _name, _value, _type = winreg.EnumValue(explorer, c)
                print("Name: %s, Val: %s, Type: %s" % (_name, _value, _type))

                if _filter is not None and _name == _filter:
                    out.append(_value)
                elif _filter is None:
                    out.append(_value)

                c += 1
        except FileNotFoundError:
            print("Could not open reg key at %s" % path)
        except WindowsError:
            pass

        return out

    def _webbrowser_open_fallback(self, name, url):
        print("Trying to open fallback for browser %s" % name)

        # Check if name is registered
        path = self.webbrowser.get_path(name)
        if path is None:
            print("Could not find path for %s" % name)
            return

        #ff_args = "--new-tab %s" % url
        ff_args = url
        chrome_args = url

        re = -1
        if name.lower() == 'chrome':
            re = subprocess.call(path % chrome_args)
        if name.lower() == 'chrome':
            re = subprocess.call(path % chrome_args)
        elif name.lower() == 'firefox':
            re = subprocess.call(path % ff_args)
        elif name.lower() == 'firefox':
            re = subprocess.call(path % ff_args)
        elif name.lower() == 'firefox':
            re = subprocess.call(path % ff_args)
        else:
            print("Could not find fallback for browser %s" % name)
            return False

        print("Return code was %s" % re)
        if re != 0:
            if re == 1 and name == 'firefox' or name == 'mozilla' or name == 'netscape':
                return True
            return False
        return True

    def _reset_csv(self):
        self.csv_data = []
        self.csv_open = False
        self.csv_index = 0
        self.csv_max = 0

    def _fix_encoding(self, string):
        u = string.encode('utf-8')
        s = u.decode('utf-8')
        return s

    #
    # Callbacks
    #

    def _cb_on_exit(self):
        self._save_value('browser', self.tkv_browser.get())
        self._save_value('entry', self.csv_index)

        fname = self.tkv_file.get()
        if fname == "Path to CSV":
            self._save_value('fname', "")
        else:
            self._save_value('fname', self.tkv_file.get())

        self._save_persistant_storage("gui.cfg")
        self.master.destroy()

    def _cb_file(self):
        initdir = os.getcwd()
        title = "Select CSV File"
        filetypes = (("CSV Files", "*.csv"), ("CSVBT Files", "*.csvbt"), ("All Files", "*.*"))

        path = filedialog.askopenfilename(initialdir=initdir, title=title, filetypes=filetypes)
        if path != "":
            self.tkv_file.set(path)

    def _cb_load(self):
        fname = self.tkv_file.get()
        if not os.path.isfile(fname):
            self._set_status("Could not open %s" % fname, "ERROR")
            return

        with open(fname, 'r', encoding='utf-8') as f:
            csv_reader = CSVProxy(f, ';')
            self._reset_csv()
            for row in csv_reader:
                self.csv_data.append(row)

        self.csv_max = len(self.csv_data)
        self.csv_open = True
        if self.csv_index > self.csv_max:
            self.csv_index = 0

        self._set_status("File sucessfully loaded!", "SUCCESS")
        self._update_csv_info_labels()

    def _cb_next(self):
        # check if csv is loaded
        if not self.csv_open:
            self._set_status("Please load a CSV file first!", "INFO")
            return
        if self.csv_index >= self.csv_max - 1:
            return

        self.csv_index += 1
        self._update_csv_info_labels()

    def _cb_prev(self):
        # check if csv is loaded
        if not self.csv_open:
            self._set_status("Please load a CSV file first!", "INFO")
            return
        if self.csv_index <= 0:
            return

        self.csv_index -= 1
        self._update_csv_info_labels()

    def _cb_open(self):
        # check if selected browser exists
        browser = None
        try:
            browser = self.webbrowser.get(self.tkv_browser.get().lower())
        except webbrowser.Error:
            self._set_status("Could not find %s browser" % self.tkv_browser.get(), "ERROR")
            return

        # Open link in new tab and check result
        res = browser.open_new_tab(self.csv_data[self.csv_index][1])
        if not res:
            print("Native webbrowser open failed")
            re = self._webbrowser_open_fallback(self.tkv_browser.get().lower(), self.csv_data[self.csv_index][1])
            if not re:
                print("Webbrowser open fallback failed")
                self._set_status("Could not open link in new tab!", "ERROR")
                return

        # check if index is at limit, if not, increment counter
        if self.csv_index == self.csv_max - 1:
            return
        self.csv_index += 1
        self._update_csv_info_labels()

# Start app
if __name__ == "__main__":
    # Configure main Window
    root = Tk()
    root.title("CSVBT v1.0")
    root.geometry("415x240")
    root.wm_iconbitmap('csvbt.ico')
    main_window = MainWindow(root)

    # Run app
    main_window.mainloop()
